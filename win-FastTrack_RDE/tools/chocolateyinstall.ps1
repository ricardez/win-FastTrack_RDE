﻿
$ErrorActionPreference = 'Stop';
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$fileLocation = Join-Path $toolsDir 'install2.cmd'
$url        = ''
$url64      = ''

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'EXE'
  url           = $url
  url64bit      = $url64
  file         = $fileLocation

  softwareName  = 'win-FastTrack_RDE*'

  checksum      = 'CFF91EFD928A85E194A1DF4798713C3C8B827F549168A5E61F932A248750D74F'
  checksumType  = 'sha256'
  checksum64    = 'CFF91EFD928A85E194A1DF4798713C3C8B827F549168A5E61F932A248750D74F'
  checksumType64= 'sha256'

  
  validExitCodes= @(0, 3010, 1641)
  silentArgs   = ''
}

Install-ChocolateyInstallPackage @packageArgs










    








