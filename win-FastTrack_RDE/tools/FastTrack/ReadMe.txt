-----------------------------------
FastTrack Schedule 7.03 - Readme.txt
AEC Software, Inc.
July 2001
-----------------------------------

(To print this document, from the File menu choose Print or open it in another 
word processor.)


IMPORTANT: Turn off automatic virus checking and close all applications before 
installing.

System Requirements
-----------------------------------
To run FastTrack Schedule 7.03 for Windows you need:
- a personal computer with a 80386/DX processor (or higher)
- Microsoft Windows Me, Windows 2000, Windows 98, Windows 95, or Microsoft 
Windows NT 4.0
- 4 MB or more of free Random Access Memory (RAM)
- 30MB or more of hard disk space
- a VGA monitor supported by your operating system
- a mouse supported by your operating system
- any printer with extended device mode drivers (made for Windows 
applications); PCL printers require at least 1MB of memory.

To run FastTrack Schedule 7.03 for Macintosh you need:
- a Macintosh Power PC (or greater)
- 4MB or more of free Random Access Memory (RAM)
- 30MB or more of hard disk space
- Macintosh System 8.6 - or higher (FastTrack Schedule is compatible with 
Mac OS X)
- a monitor supported by your operating system
- a mouse supported by your operating system
- any printer with a Chooser-level driver


Installing
-----------------------------------
As with any software installation, turn off automatic virus checking and 
close all applications before installing.

To install FastTrack Schedule 7.03 for Windows:
From CD-ROM-
1. Insert the FastTrack Schedule CD-ROM into the CD-ROM drive.
2. Wait for a dialog to appear automatically.
3. Click the "Install FastTrack Schedule" button.
4. Follow the instructions on screen.

[*If the CD Auto Run does not work, do the following:
1. Insert CD-ROM into your CD-ROM drive.
2. From the Start menu, choose Run.
3. Type "d:\install\disk1\setup.exe" and click OK. (Substitute the letter 
of your CD-ROM drive, such as "e:\install\disk1\setup").
4. Follow the instructions on screen.]


To install FastTrack Schedule 7.03 for Macintosh:
From CD-ROM-
1. Insert the FastTrack Schedule CD-ROM into the CD-ROM drive.
2. Double-click the FastTrack Schedule icon when it appears on your 
desktop.
3. Double-click the "FastTrack Schedule Install" icon to run the 
installer.
4. Follow the instructions on screen.


Installing on a Network
-----------------------------------
To install FastTrack Schedule 7.03 for Windows on a Network:
1. Log in as the System Administrator (or its equivalent) for your 
network.
2. Connect to the destination server or Shared Directory/Folder where you 
wish FastTrack Schedule to be installed.
3. Follow the directions provided above to run the installer.
4. Follow the instructions on screen to navigate to the desired directory 
and install FastTrack Schedule 7.03.

Once installation is complete, the network administrator needs to:
1. Enter the Network version Key Code.
2. Set the application to sharable. Consult your network operating system 
documentation for the specifics of this procedure, as it varies from 
network to network.
3. Confirm that the AppData directory within the FastTrack Schedule 
directory has Read-Write privileges. Consult your network operating 
system documentation for the specifics of this procedure, as it varies 
from network to network.


To install FastTrack Schedule 7.03 for Macintosh on a Network:
1. Log in as the System Administrator (or its equivalent) for your 
network.
2. Connect to the destination server or Shared Directory/Folder where you 
wish FastTrack Schedule to be installed.
3. Follow the directions provided above to run the installer.
4. Follow the instructions on screen to navigate to the desired directory 
and install FastTrack Schedule 7.03.

Once installation is complete, the network administrator 
needs to:
1. Enter the Network version Key Code.
2. Set the FastTrack Schedule 7.03 application to sharable. Consult your 
network operating system documentation for the specifics of this procedure, 
as it varies from network to network.
3. Confirm that the AppData directory within the FastTrack Schedule 7.0 
directory (default named FastTrack Schedule 7.0 by the installer) has 
Read/Write privileges. Consult your network operating system documentation 
for the specifics of this procedure, as it varies from network to network.



Demo & Full Application
-----------------------------------
When you install FastTrack Schedule 7.03, you are installing both the 
fully enabled application and the Demo version. 
The demo version is a two (2) user network version whose only limitation 
is not allowing you to save schedule files. 
You can install it on your computer or on a server or shared directory/
folder.

-If you have purchased the program, enter the Serial # (found on the box, 
the CD envelope, and registration card) when asked to do so. This will 
enable all features for FastTrack Schedule 7.03.

-If you have received the program as a demonstration version, no Serial # 
will be provided and you should instead type "Demo" when asked to enter 
the Serial #. This gives you all the power and flexibility of the full 
FastTrack Schedule --the only thing you cannot do is save schedule files. 
If after evaluating the demo, you would like to purchase the program, 
contact our Sales Department at (800)346-9413 to receive a Serial #. 

-To convert the Demo version into a fully enabled program, open the About 
FastTrack Schedule dialog (located in the Help menu [Windows] or the 
Apple menu [Macintosh])and click the "Enable..." button. Enter your 
Serial # in place of the word Demo, click OK, and you now have a fully 
enabled copy of FastTrack Schedule.

-Each serial number for this product is valid for one (1) platform only. 
Consult the original packaging to learn the platform for which your 
serial number is licensed.


Required Printer & Video Drivers for Windows 95, Windows 98, 
Windows 2000, Windows Me, and Windows NT 4.0
-----------------------------------
FastTrack Schedule for Windows was designed as a full 32-bit product for 
Windows.

Many printer manufacturers and video manufacturers need to provide updated 
drivers for the Windows 95, Windows 98, Windows 2000, Windows Me, and 
Windows NT 4.0. Some have been more prompt than others in addressing these 
issues. 
As this is a world-wide issue, users should check with the manufacturers of 
their printers and video drivers for these environments. A convenient way 
to do so is to check the manufacturer's Internet web site for updated 
drivers, as most manufacturers now make these updated drivers available 
free of charge.


***Note to Upgrading Windows Users***
--------------------------------------
IMPORTANT: We've made major enhancements to our VBA object model that 
expose much more of the program's functionality for use in constructing 
VBA scripts. 
As a result, it will be necessary to update all VBA scripts written 
prior to version 7.0 of FastTrack Schedule.


What's New in 7.03?
---------------------------
FastTrack Schedule 7.03 is a maintenance release, focused on addressing 
some unexpected behavior, fixing minor bugs, and making minor enhancements 
to existing features.

Enhanced Features:
- Expand All and Collapse All - Provided Expand All and Collapse All 
buttons in the Standard palette in lieu of Expand Activity Row and 
Collapse Activity Row.
- Enhanced MPX functionality
- Added Column Auto-Fit Width for Row Number Action Column
- Enhancements to Layouts and FastSteps
- Modified behavior of spot allocations in Resource View
- Numerous minor usability changes requested by users

Bug Fixes:
- Modification of linking and link dependencies.
- Enhanced the print preview of wall charts, and other print related 
issues.
- Restored compatibility with OS 9 Multi-user environment-Macintosh 
only.

 

[FastTrack Schedule 7.03 will open files from version 5.0 and higher. 
If you need to open files prior to version 5.0, contact AEC Software 
Customer Service at 703-450-1980 or service@aecsoft.com]


Contacting AEC Software, Inc.
-----------------------------------
Phone Hours: 8:00AM - 5:00PM, Eastern Time, USA

- To get help with the program, call our technical support staff at 
(703)450-2318

- To fax us information, use (703)450-9786

- To visit our web site (for late breaking info, downloadable 
examples, and free program updates), our address is: http://www.aecsoft.com 

- To contact us via e-mail: 
Sales - sales@aecsoft.com  
Technical Support -  support@aecsoft.com
Program Comments, Suggestions, & Feedback - feedback@aecsoft.com 
Customer Service - service@aecsoft.com


[FTS7.03-072401.1]
---------------------------------------------
(c) Copyright AEC Software, Inc. 2001
