#define Function_Reference	12001
#define Function_Reference_alpha	12002
#define Number_Functions	12003
#define Abs_Funct	12004
#define Cos_Function	12005
#define Div_Function	12006
#define Even_Function	12007
#define FV_Future_Value_Function	12008
#define Ln_Function	12009
#define Log_Function	12010
#define Mod_Function	12011
#define NullNum_Function	12012
#define Odd_Function	12013
#define Pmt_Payment_Function	12014
#define PV_Present_Value_Function	12015
#define Round_Function	12016
#define Sin_Function	12017
#define Sqrt_Function	12018
#define Tan_Function	12019
#define Trunc_Function	12020
#define Text_Functions	12021
#define DateToText_Function	12022
#define Find_Function	12023
#define FindPos_Function	12024
#define Left_Function	12025
#define LeftWords_Function	12026
#define Mid_Function	12027
#define MidWords_Function	12028
#define NumChars_Function	12029
#define NumToText_Function	12030
#define NumWords_Function	12031
#define Proper_Function	12032
#define Replace_Function	12033
#define Right_Function	12034
#define RightWords_Function	12035
#define TextToDate_Function	12036
#define TextToNum_Function	12037
#define TextToTime_Function	12038
#define TimeToText_Function	12039
#define ToLower_Function	12040
#define ToUpper_Function	12041
#define Trim_Function	12042
#define Statistical_Functions	12043
#define Ave_Function	12044
#define AveDev_Function	12045
#define Max_Function	12046
#define Min_Function	12047
#define StdDev_Function	12048
#define Logical_Functions	12049
#define If_Function	12050
#define IsEven_Function	12051
#define IsInt_Function	12052
#define IsNull_Function	12053
#define IsOdd_Function	12054
#define TextIsDate_Function	12055
#define TextIsNum_Function	12056
#define TextIsTime_Function	12057
#define Date_and_Time_Functions	12058
#define Date_Function	12059
#define Day_Function	12060
#define DayofWeek_Function	12061
#define DayofWeekName_Function	12062
#define Duration_Function	12063
#define Month_Function	12065
#define MonthName_Function	12066
#define ScheduleDate_Function	12067
#define ScheduleTime_Function	12068
#define Time_Function	12069
#define Year_Function	12070
#define Operator_Reference	12071
#define Mathematical_Operators	12072
#define Text_Operators	12073
#define Comparison_Operators	12074
#define Logical_Operators	12075
#define Dur_w_Times_Funct	12064
#define ExpFunction	12076
#define FactFunction	12077
#define PowerFunction	12078
#define FinishDateFunction	12079
#define FinishDatewTimesFunction	12080
#define WrkCalBsDyDurFunction	12081
#define WrkCalTypDayDurFunc	12082
#define WrkCalDayDurFunc	12083
