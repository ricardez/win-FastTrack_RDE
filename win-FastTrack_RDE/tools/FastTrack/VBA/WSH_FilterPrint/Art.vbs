'***************************************************************************
' FastTrack Schedule 7.0 Example Macros
'
'DISCLAIMER OF WARRANTY
'
'THE SAMPLE DESCRIBED IN THIS DOCUMENT IS UNDOCUMENTED SAMPLE CODE.
'THIS SAMPLE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND.
'AEC SOFTWARE FURTHER DISCLAIMS ALL IMPLIED WARRANTIES INCLUDING WITHOUT
'LIMITATION ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR OF FITNESS
'FOR A PARTICULAR PURPOSE. THE ENTIRE RISK ARISING OUT OF THE USE OR
'PERFORMANCE OF THE SAMPLES REMAINS WITH YOU.
'
'IN NO EVENT SHALL AEC SOFTWARE OR ITS SUPPLIERS BE LIABLE FOR ANY DAMAGES
'WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF
'BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION,
'OR OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF OR INABILITY TO USE
'THE SAMPLES, EVEN IF AEC SOFTWARE HAS BEEN ADVISED OF THE POSSIBILITY OF
'SUCH DAMAGES. BECAUSE SOME STATES DO NOT ALLOW THE EXCLUSION OR
'LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE
'ABOVE LIMITATION MAY NOT APPLY TO YOU.
'
' Refer to VBA Help in FastTrack for help on FastTrack Schedule
' Objects.
'
' 9.15.2000 AEC Software
'***************************************************************************

'-------------------------------------------------------------------------
' Windows Scripting Host Example
'
' This script will open the example file, 'Art.fts', switch to a particular layout,
' provide an option to print the schedule after completing each of three different filters,
' then close the example file.
'
' Note: Windows Scripting Host(WSH) is available in Windows 98 only, or if you have
' explicitly installed it on Windows 95/NT.
'-------------------------------------------------------------------------

Option Explicit

Dim aFastTrack
Dim aArtDoc
Const MsgBox_Title_Text = "Print Projects"

Call Confirm

On Error Resume Next

'Create the 'FastTrack' and 'FastTrack Document' objects.
Set aFastTrack = WScript.CreateObject("FastTrack.Application.7")
'FastTrack.Visible = True
Set aArtDoc = aFastTrack.Documents.open(aFastTrack.Path & "VBA\WSH_FilterPrint\Art.fts")
If Err.Number <> 0 Then
    Msgbox "Error opening document!"
End If

'Go to a particular layout.
aArtDoc.Layout("Activity Names, Dates, & Times")

'Filter and Print(optional) the schedule.
Call Filter("Kim's Projects")
Call Filter("Mike's Projects")
Call Filter("Charlie's Projects")

'-------------------------------------------------------------------------
' Sub procedure, Confirm
' This procedure shows a confirmation dialog before the FastTrack application is run.
'-------------------------------------------------------------------------
Sub Confirm()
	const MsgBox_Message_Text = "Continue printing each artist's projects?"

    Dim intDoIt
    intDoIt = MsgBox(MsgBox_Message_Text, vbOKCancel + vbInformation, MsgBox_Title_Text)
    If intDoIt = vbCancel Then
        WScript.Quit
    End If
End Sub

'-------------------------------------------------------------------------
' Sub procedure, Filter
' This procedure filters the schedule according to the artist then prompts
' the user to print.
'-------------------------------------------------------------------------
Sub Filter(FilterName)
	Dim DoPrint

	aArtDoc.Filter(FilterName)
	DoPrint = MsgBox("Print " & FilterName, vbOKCancel + vbInformation, MsgBox_Title_Text)
	If DoPrint = vbOK Then
		aArtDoc.ExecuteFastStep ("Print Alert")
		'aArtDoc.PrintOut
	End If
	aArtDoc.RestoreAllRows
End Sub
