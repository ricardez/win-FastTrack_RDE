'***************************************************************************
' FastTrack Schedule 7.0 Example Macros
'
'DISCLAIMER OF WARRANTY
'
'THE SAMPLE DESCRIBED IN THIS DOCUMENT IS UNDOCUMENTED SAMPLE CODE.
'THIS SAMPLE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND.
'AEC SOFTWARE FURTHER DISCLAIMS ALL IMPLIED WARRANTIES INCLUDING WITHOUT
'LIMITATION ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR OF FITNESS
'FOR A PARTICULAR PURPOSE. THE ENTIRE RISK ARISING OUT OF THE USE OR
'PERFORMANCE OF THE SAMPLES REMAINS WITH YOU.
'
'IN NO EVENT SHALL AEC SOFTWARE OR ITS SUPPLIERS BE LIABLE FOR ANY DAMAGES
'WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF
'BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION,
'OR OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF OR INABILITY TO USE
'THE SAMPLES, EVEN IF AEC SOFTWARE HAS BEEN ADVISED OF THE POSSIBILITY OF
'SUCH DAMAGES. BECAUSE SOME STATES DO NOT ALLOW THE EXCLUSION OR
'LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE
'ABOVE LIMITATION MAY NOT APPLY TO YOU.
'
' Refer to VBA Help in FastTrack for help on FastTrack Schedule
' Objects.
'
' 9.15.2000 AEC Software
'***************************************************************************

'-------------------------------------------------------------------------
' Windows Scripting Host Example
'
' This Visual Basic Script will create a new FastTrack schedule document and
' insert activities obtained from a Microsoft Access database.
'
' Note: Windows Scripting Host(WSH) is available in Windows 98, or if you have
' explicitly installed it on Windows 95/NT. Also Microsoft Access 95 and 
' Visual Basic 5.0 are required to run this example.
'-------------------------------------------------------------------------

Option Explicit

Const MsgBox_Title_Text = "Database Access"
Const strSQL = "SELECT EventName, StartDate, EndDate FROM Events"

Dim aFastTrack
Dim FTSDoc
Dim aActivity, aBar

Dim DbEngine
Dim Dbase
Dim rst

Call Confirm

On Error Resume Next

'Make reference to the database engine.
Set DbEngine = WScript.CreateObject("DAO.DBEngine.36")
If DbEngine is nothing Then
    Msgbox "Error getting DBEngine!"
End If

'Open the database.
Set Dbase = DbEngine.Workspaces(0).OpenDatabase("Event Shedule.mdb")
If Err.Number <> 0 Then
    Msgbox "Error getting Database!"
End If

'Create a recordset from the SQL statement.
Set rst = Dbase.OpenRecordset(strSQL)
If Err.Number <> 0 Then
    Msgbox "Error opening recordset!"
End If

'Create the 'FastTrack' and 'FastTrack Document' objects.
Set aFastTrack = WScript.CreateObject("FastTrack.Application.7")

'Create a new document.
aFastTrack.Visible = True
aFastTrack.New
Set FTSDoc = aFastTrack.ActiveDocument

'Loop through the recordset setting the activity rows in FastTrack
'to the values stored in the database.
Dim x
x = 1
Do While Not rst.EOF
	'add and activity and set its name
	Set aActivity = FTSDoc.Activities.Add
	aActivity.Name = rst("EventName")

	'add and bar and set the dates
	Set aBar = aActivity.Bars.Add
	aBar.StartDate = rst("StartDate")
	aBar.FinishDate = rst("EndDate")

    rst.MoveNext
	x = x + 1
Loop

rst.Close
Dbase.Close

'FTSDoc.Close

'-------------------------------------------------------------------------
' Sub procedure, Confirm
' This procedure shows a confirmation dialog before the FastTrack application is run.
'-------------------------------------------------------------------------
Sub Confirm()
	const MsgBox_Message_Text = "Import Data to FastTrack Schedule?"

    Dim intDoIt
    intDoIt = MsgBox(MsgBox_Message_Text, vbOKCancel + vbInformation, MsgBox_Title_Text)
    If intDoIt = vbCancel Then
        WScript.Quit
    End If
End Sub

